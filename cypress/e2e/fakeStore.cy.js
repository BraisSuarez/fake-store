describe('template spec', () => {
  it('passes', () => {
    cy.viewport(1536, 960);
    cy.visit('http://localhost:8080/');
    cy.wait(1500);
    cy.get('#clothingPic').click();
    cy.get('[data-id="1"] > img').click();
    cy.get('#storeIcon').click();
    cy.wait(1500);
    cy.get('#jewelryPic').click();
    cy.get('[data-id="5"] > img').click();
    cy.get('#storeIcon').click();
    cy.wait(1500);
    cy.get('#techPic').click();
    cy.get('[data-id="9"] > img').click();
    cy.wait(1500);
    cy.get('[href="/clothes"]').click();
    cy.wait(1500);
    cy.get('[href="/jewelry"]').click();
    cy.wait(1500);
    cy.get('[href="/tech"]').click();
  });
});
