import { LitElement, html } from 'lit';
import { AllProductsUseCase } from '../usecases/getAllProducts.usecase';
import { selectedProduct } from '../states/state';
import { Router } from '@vaadin/router';

export class ClothesComponent extends LitElement {
  static get properties() {
    return {
      products: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.products = await AllProductsUseCase.execute();
  }

  render() {
    return html`
      <ul class="clothes-products">
        ${this.products?.map((product) =>
          product.category === "men's clothing" ||
          product.category === "women's clothing"
            ? html`<li data-id=${product.id} @click=${this.productHandler}>
                <img src="${product.image}" />
                </li>
                <h2 class="product-name">${product.name}</h2></br>
                <span class="product-price">${product.price}$</span> 
                `
            : ''
        )}
      </ul>
    `;
  }

  productHandler(e) {
    e.preventDefault();

    const productId = parseInt(e.currentTarget.dataset.id);

    const isolatedProduct = this.products.filter(
      (product) => product.id === productId
    );

    selectedProduct.garment = isolatedProduct;

    Router.go('/garment');
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('clothes-component', ClothesComponent);
