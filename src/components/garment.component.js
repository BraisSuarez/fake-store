import { LitElement, html } from 'lit';
import { selectedProduct } from '../states/state';

export class GarmentComponent extends LitElement {
  static get properties() {
    return {
      garment: { type: Object },
    };
  }

  connectedCallback() {
    super.connectedCallback();

    const media = window.matchMedia('(min-width: 1000px)');

    const headerIcon = document.getElementById('storeIcon');
    if (!media.matches) {
      headerIcon.style.height = '13rem';
    }

    this.garment = selectedProduct.garment[0];
  }

  disconnectedCallback() {
    const media = window.matchMedia('(min-width: 1000px)');

    const headerIcon = document.getElementById('storeIcon');

    if (!media.matches) {
      headerIcon.style.height = '19rem';
    }
  }

  render() {
    return html` 
      <section class="section-garment">
      <img class="garment-img" src="${this.garment?.image}" />
      <h2 class="garment-h2">${this.garment?.name}</h2></br>
      <div class="module tooManyWords"><p>${this.garment?.description}</p></div>
      <span>${this.garment?.price}$</span>
      </section> 
      `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('garment-component', GarmentComponent);
