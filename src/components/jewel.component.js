import { LitElement, html } from 'lit';
import { jewelProduct } from '../states/state';

export class JewelComponent extends LitElement {
  static get properties() {
    return {
      jewel: { type: Object },
    };
  }

  connectedCallback() {
    super.connectedCallback();

    const media = window.matchMedia('(min-width: 1000px)');

    const headerIcon = document.getElementById('storeIcon');
    if (!media.matches) {
      headerIcon.style.height = '13rem';
    }

    this.jewel = jewelProduct.jewel[0];
  }

  disconnectedCallback() {
    const media = window.matchMedia('(min-width: 1000px)');

    const headerIcon = document.getElementById('storeIcon');

    if (!media.matches) {
      headerIcon.style.height = '19rem';
    }
  }

  render() {
    return html` 
      <section class="section-jewel">
      <img class="jewel-image" src="${this.jewel?.image}" />
      <h2>${this.jewel?.title}</h2></br>
      <p>${this.jewel?.description}</p>
      <span>${this.jewel?.price}$</span>
      </section> 
      `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('jewel-component', JewelComponent);
