import { LitElement, html } from 'lit';
import { SpecificProducts } from '../usecases/specificProducts.usecase';
import { jewelProduct } from '../states/state';
import { Router } from '@vaadin/router';

export class JeweleryComponent extends LitElement {
  static get properties() {
    return {
      products: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.products = await SpecificProducts.execute('jewelery');
  }

  render() {
    return html`
      <ul class="jewelry-products">
        ${this.products?.map(
          (
            product
          ) => html`<li data-id=${product.id} @click=${this.productHandler}>
                  <img src="${product.image}" />
                </li>
                <h2 class="product-name">${product.title}</h2></br>
                <span class="product-price">${product.price}$</span>  `
        )}
      </ul>
    `;
  }

  productHandler(e) {
    e.preventDefault();

    const productId = parseInt(e.currentTarget.dataset.id);

    const isolatedProduct = this.products.filter(
      (product) => product.id === productId
    );

    jewelProduct.jewel = isolatedProduct;

    Router.go('/jewel');
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('jewelry-component', JeweleryComponent);
