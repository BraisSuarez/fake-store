import { LitElement, html } from 'lit';
import imgClothing from '../media/clothing.png';
import imgJewelry from '../media/jewelry.png';
import imgTech from '../media/tech.png';

export class SelectorComponent extends LitElement {
  render() {
    return html`
      <ul class="carousel">
        <li>
          <p class="clothing-text">Clothing</p>
          <a href="/clothes"
            ><img src="${imgClothing}" alt="clothing image" id="clothingPic"
          /></a>
        </li>
        <li>
          <p class="jewelry-text">Jewelry</p>
          <a href="/jewelry"
            ><img src="${imgJewelry}" alt="clothing image" id="jewelryPic"
          /></a>
        </li>
        <li>
          <p class="tech-text">Tech</p>
          <a href="/tech"
            ><img src="${imgTech}" alt="clothing image" id="techPic"
          /></a>
        </li>
      </ul>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('selector-component', SelectorComponent);
