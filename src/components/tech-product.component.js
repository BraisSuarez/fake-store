import { LitElement, html } from 'lit';
import { techProduct } from '../states/state';

export class TechProductComponent extends LitElement {
  static get properties() {
    return {
      tech: { type: Object },
    };
  }

  connectedCallback() {
    super.connectedCallback();

    const media = window.matchMedia('(min-width: 1000px)');

    const headerIcon = document.getElementById('storeIcon');
    if (!media.matches) {
      headerIcon.style.height = '13rem';
    }

    this.tech = techProduct.tech[0];
  }

  disconnectedCallback() {
    const media = window.matchMedia('(min-width: 1000px)');

    const headerIcon = document.getElementById('storeIcon');

    if (!media.matches) {
      headerIcon.style.height = '19rem';
    }
  }

  render() {
    return html` 
      <section class="section-tech">
      <img class="tech-image" src="${this.tech?.image}" />
      <h2>${this.tech?.title}</h2></br>
      <p>${this.tech?.description}</p>
      <span>${this.tech?.price}$</span>
      </section> 
      `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('isolated-tech-component', TechProductComponent);
