import { LitElement, html } from 'lit';
import { SpecificProducts } from '../usecases/specificProducts.usecase';
import { Router } from '@vaadin/router';
import { techProduct } from '../states/state';

export class TechComponent extends LitElement {
  static get properties() {
    return {
      products: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.products = await SpecificProducts.execute('electronics');
  }

  render() {
    return html`
      <ul class="tech-products">
        ${this.products?.map((product) =>
          product.category === 'electronics'
            ? html`<li data-id=${product.id} @click=${this.productHandler}>
                  <img src="${product.image}" />
                </li>
                <h2 class="product-name">${product.title}</h2></br>
                <span class="product-price">${product.price}$</span>`
            : ''
        )}
      </ul>
    `;
  }

  productHandler(e) {
    e.preventDefault();

    const productId = parseInt(e.currentTarget.dataset.id);

    const isolatedProduct = this.products.filter(
      (product) => product.id === productId
    );

    techProduct.tech = isolatedProduct;

    Router.go('/tech-product');
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('tech-component', TechComponent);
