import './main.css';
import './pages/home.page';
import './pages/jewelry.page';
import './pages/tech.page';
import './pages/clothes.page';
import './pages/garment.page';
import './pages/jewel.page';
import './pages/tech-product.page';

import { Router } from '@vaadin/router';

const outlet = document.getElementById('outlet');
const router = new Router(outlet);
router.setRoutes([
  { path: '/', component: 'home-page' },
  { path: '/jewelry', component: 'jewelry-page' },
  { path: '/tech', component: 'tech-page' },
  { path: '/clothes', component: 'clothes-page' },
  { path: '/garment', component: 'garment-page' },
  { path: '/jewel', component: 'jewel-page' },
  { path: '/tech-product', component: 'tech-product-page' },
  { path: '(.*)', redirect: '/' },
]);
