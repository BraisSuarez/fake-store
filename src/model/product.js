export class Product {
  constructor({ id, name, price, category, description, image, rating }) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.category = category;
    this.description = description;
    this.image = image;
    this.rating = rating;
  }
}
