import '../components/clothes.component';

export class ClothesPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <clothes-component></clothes-component>
      `;
  }
}

customElements.define('clothes-page', ClothesPage);
