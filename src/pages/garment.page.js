import '../components/garment.component';

export class GarmentPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <garment-component></garment-component>
      `;
  }
}

customElements.define('garment-page', GarmentPage);
