import '../components/selector.component';

import arrowDown from '../media/arrowDown.png';

export class HomePage extends HTMLElement {
  async connectedCallback() {
    this.innerHTML = `
      <selector-component></selector-component>
      <footer>
        <img src="${arrowDown}" class="arrow heartbeat" />
      </footer>
    `;
  }
}

customElements.define('home-page', HomePage);
