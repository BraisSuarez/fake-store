import '../components/jewel.component';

export class JewelPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <jewel-component></jewel-component>
      `;
  }
}

customElements.define('jewel-page', JewelPage);
