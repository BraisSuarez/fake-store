import '../components/jewelry.component';

export class JewelryPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <jewelry-component></jewelry-component>
      `;
  }
}

customElements.define('jewelry-page', JewelryPage);
