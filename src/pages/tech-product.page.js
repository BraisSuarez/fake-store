import '../components/tech-product.component';

export class TechProductPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <isolated-tech-component></isolated-tech-component>
      `;
  }
}

customElements.define('tech-product-page', TechProductPage);
