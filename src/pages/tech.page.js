import '../components/tech.component';

export class TechPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <tech-component></tech-component>
      `;
  }
}

customElements.define('tech-page', TechPage);
