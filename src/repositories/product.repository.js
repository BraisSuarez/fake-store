import axios from 'axios';

export class ProductsRepository {
  async getAllProducts() {
    return await (
      await axios.get('https://fakestoreapi.com/products')
    ).data;
  }

  async specificProducts(category) {
    return await (
      await axios.get(`https://fakestoreapi.com/products/category/${category}`)
    ).data;
  }
}
