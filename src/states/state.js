import { proxy } from 'valtio';

export const selectedProduct = proxy({ garment: {} });
export const jewelProduct = proxy({ jewel: {} });
export const techProduct = proxy({ tech: {} });
