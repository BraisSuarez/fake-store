import { ProductsRepository } from '../repositories/product.repository';
import { Product } from '../model/product';

export class AllProductsUseCase {
  static async execute() {
    const repository = new ProductsRepository();
    const products = await repository.getAllProducts();

    return products.map(
      (product) =>
        new Product({
          id: product.id,
          name: product.title,
          price: product.price,
          category: product.category,
          description: product.description,
          image: product.image,
          rating: product.rating,
        })
    );
  }
}
