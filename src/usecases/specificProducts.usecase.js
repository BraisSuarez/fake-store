import { ProductsRepository } from '../repositories/product.repository';
import { Product } from '../model/product';

export class SpecificProducts {
  static async execute(category) {
    const repository = new ProductsRepository();
    const products = await repository.specificProducts(category);

    return products.filter((product) => {
      if (product.category === category) {
        return new Product({
          id: product.id,
          name: product.title,
          price: product.price,
          category: product.category,
          description: product.description,
          image: product.image,
          rating: product.rating,
        });
      }
    });
  }
}
