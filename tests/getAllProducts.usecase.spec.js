import { AllProductsUseCase } from '../src/usecases/getAllProducts.usecase';
import { ProductsRepository } from '../src/repositories/product.repository';
import { PRODUCTS } from './fixtures/products';

jest.mock('../src/repositories/product.repository.js');

describe('All product usecase', () => {
  beforeEach(() => {
    ProductsRepository.mockClear();
  });
  it('shouldreceive all products', async () => {
    // Arrange
    ProductsRepository.mockImplementation(() => {
      return {
        getAllProducts: () => {
          return PRODUCTS;
        },
      };
    });

    // Act
    const products = await AllProductsUseCase.execute();

    // Assert
    expect(products.length).toBe(20);
    expect(products[0].name).toBe(PRODUCTS[0].title);
    expect(products[0].description).toBe(PRODUCTS[0].description);
  });
});
