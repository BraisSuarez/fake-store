import { ProductsRepository } from '../src/repositories/product.repository';
import { SpecificProducts } from '../src/usecases/specificProducts.usecase';

jest.mock('../src/repositories/product.repository.js');

describe('All product usecase', () => {
  beforeEach(() => {
    ProductsRepository.mockClear();
  });
  it('shouldreceive all products', async () => {
    // Arrange
    const category = 'jewelery';

    const products = [
      {
        id: 4,
        title: 'Mens Casual Slim Fit',
        price: 15.99,
        category: "men's clothing",
        description:
          'The color could be slightly different between on the screen and in practice. / Please note that body builds vary by person, therefore, detailed size information should be reviewed below on the product description.',
        image: 'https://fakestoreapi.com/img/71YXzeOuslL._AC_UY879_.jpg',
        rating: { rate: 2.1, count: 430 },
      },
      {
        id: 5,
        title:
          "John Hardy Women's Legends Naga Gold & Silver Dragon Station Chain Bracelet",
        price: 695,
        category: 'jewelery',
        description:
          "From our Legends Collection, the Naga was inspired by the mythical water dragon that protects the ocean's pearl. Wear facing inward to be bestowed with love and abundance, or outward for protection.",
        image:
          'https://fakestoreapi.com/img/71pWzhdJNwL._AC_UL640_QL65_ML3_.jpg',
        rating: { rate: 4.6, count: 400 },
      },
    ];

    ProductsRepository.mockImplementation(() => {
      return {
        specificProducts: () => {
          return products;
        },
      };
    });

    // Act
    const specificProducts = await SpecificProducts.execute(category);

    // Assert
    expect(specificProducts[0].category).toBe(category);
    expect(specificProducts.length).toBe(1);
  });
});
